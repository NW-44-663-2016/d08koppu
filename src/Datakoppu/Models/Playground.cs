﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Datakoppu.Models
{
    public class Playground
    {
         [ScaffoldColumn(false)]
         public int PlaygroundID { get; set; }

         [Display(Name = "Ground Name")]
         public String GroundName { get; set; }

         [Display(Name = "Category")]
         public String Category { get; set; }

         [Range(1600, 3000)]
         [Display(Name = "Year Established")]
         public Double EstablishmentYear { get; set; }

         [Display(Name = "Email ID")]
         [DataType(DataType.EmailAddress)]
         public string EmailID { get; set; }

         [RegularExpression("\\(?\\d{3}\\)?-? *\\d{3}-? *-?\\d{4}")]
         [Display(Name = "Phone Number")]
         public String PhoneNumber { get; set; }

        [ScaffoldColumn(false)]
        public Int32? LocationID { get; set; }

         [Display(Name = "Location")]
         public virtual Location Location { get; set; }

         [Range(30000, 200000)]
         [Display(Name = "Capacity")]
         public Double Capacity { get; set; }

         [Display(Name = "Website")]
         [DataType(DataType.Url)]
         public string URL { get; set; }

        public static List<Playground> ReadAllFromCSV(string filepath)
        {
            List<Playground> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Playground.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Playground OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Playground item = new Playground();
            int i = 0;
            item.GroundName = Convert.ToString(values[i++]);
            item.Category = Convert.ToString(values[i++]);
            item.Capacity = Convert.ToDouble(values[i++]);
            item.EstablishmentYear = Convert.ToDouble(values[i++]);
            item.EmailID = Convert.ToString(values[i++]);
            item.PhoneNumber = Convert.ToString(values[i++]);
            item.URL = Convert.ToString(values[i++]);
           
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }




    }
}
