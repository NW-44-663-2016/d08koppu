﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace Datakoppu.Models
{
    public class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Locations.RemoveRange(context.Locations);
            context.Playgrounds.RemoveRange(context.Playgrounds);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedPlaygroundsFromCsv(relPath, context);

           // var context = serviceProvider.GetService<AppDbContext>();
           // if (context.Database == null)
           // {
           //     throw new Exception("DB is null");
           // }
           // if (context.Locations.Any())
           // {
           //     return;
           // }

           
           // var l1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
           // var l2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
           // var l3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
           // var l4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
           // var l5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
           // var l6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
           // context.Locations.AddRange(l1, l2, l3, l4, l5, l6);

            

           //context.Playgrounds.AddRange(
           // new Playground() { GroundName = "MCG", Category = "Cricket", EstablishmentYear = 1890, Capacity = 60000, PhoneNumber = "9866718299", EmailID = "Cricket.Au@org"  , LocationID = l1.LocationID},
           // new Playground() { GroundName = "Jamica", Category = "Cricket", EstablishmentYear = 1860, Capacity = 70000, PhoneNumber = "9877718299", EmailID = "Cricket.Pcb@org" , LocationID = l2.LocationID },
           // new Playground() { GroundName = "Gabba", Category = "Cricket", EstablishmentYear = 1894, Capacity = 80000, PhoneNumber = "9866718259", EmailID = "Cricket.Wi@org" , LocationID = l3.LocationID },
           // new Playground() { GroundName = "Colombo", Category = "Cricket", EstablishmentYear = 1895, Capacity = 90000, PhoneNumber = "9866718269", EmailID = "Cricket.Sa@org" , LocationID = l4.LocationID },
           // new Playground() { GroundName = "La", Category = "Cricket", EstablishmentYear = 1896, Capacity = 40000, PhoneNumber = "9866718279", EmailID = "Cricket.Ind@org" , LocationID = l5.LocationID },
           // new Playground() { GroundName = "TA", Category = "Cricket", EstablishmentYear = 1840, Capacity = 50000, PhoneNumber = "9866718239", EmailID = "Cricket.Sl@org" , LocationID = l6.LocationID }
           //  );

           // context.SaveChanges();
        }

        private static void SeedPlaygroundsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "Playground.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Playground.ReadAllFromCSV(source);
            List<Playground> lst = Playground.ReadAllFromCSV(source);
            context.Playgrounds.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }

    }
}
