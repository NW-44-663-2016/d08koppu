using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using Datakoppu.Models;

namespace Datakoppu.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222060306_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Datakoppu.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("Datakoppu.Models.Playground", b =>
                {
                    b.Property<int>("PlaygroundID")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Capacity");

                    b.Property<string>("Category");

                    b.Property<string>("EmailID");

                    b.Property<double>("EstablishmentYear");

                    b.Property<string>("GroundName");

                    b.Property<int?>("LocationID");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("URL");

                    b.HasKey("PlaygroundID");
                });

            modelBuilder.Entity("Datakoppu.Models.Playground", b =>
                {
                    b.HasOne("Datakoppu.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
