using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Datakoppu.Models;

namespace Datakoppu.Controllers
{
    [Produces("application/json")]
    [Route("api/Playgrounds")]
    public class PlaygroundsController : Controller
    {
        private AppDbContext _context;

        public PlaygroundsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Playgrounds
        [HttpGet]
        public IEnumerable<Playground> GetPlaygrounds()
        {
            return _context.Playgrounds;
        }

        // GET: api/Playgrounds/5
        [HttpGet("{id}", Name = "GetPlayground")]
        public IActionResult GetPlayground([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Playground playground = _context.Playgrounds.Single(m => m.PlaygroundID == id);

            if (playground == null)
            {
                return HttpNotFound();
            }

            return Ok(playground);
        }

        // PUT: api/Playgrounds/5
        [HttpPut("{id}")]
        public IActionResult PutPlayground(int id, [FromBody] Playground playground)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != playground.PlaygroundID)
            {
                return HttpBadRequest();
            }

            _context.Entry(playground).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaygroundExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Playgrounds
        [HttpPost]
        public IActionResult PostPlayground([FromBody] Playground playground)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Playgrounds.Add(playground);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PlaygroundExists(playground.PlaygroundID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetPlayground", new { id = playground.PlaygroundID }, playground);
        }

        // DELETE: api/Playgrounds/5
        [HttpDelete("{id}")]
        public IActionResult DeletePlayground(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Playground playground = _context.Playgrounds.Single(m => m.PlaygroundID == id);
            if (playground == null)
            {
                return HttpNotFound();
            }

            _context.Playgrounds.Remove(playground);
            _context.SaveChanges();

            return Ok(playground);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlaygroundExists(int id)
        {
            return _context.Playgrounds.Count(e => e.PlaygroundID == id) > 0;
        }
    }
}